<!-- Scroll to Top Button-->
<button class="scroll-to-top rounded" data-target="#page-top">
    <i class="fa fa-angle-up"></i>
</button>
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

<script src="assets/scripts/global.js"></script>
<script src="assets/dist/fontawesome/js/all.min.js"></script>
