<?php


namespace App\Entity;

use App\Core\Interfaces\Entity;

class User
{
    use Entity;

    protected $name;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }


}